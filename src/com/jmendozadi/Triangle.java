package com.jmendozadi;

public class Triangle extends Geometric {
    public Triangle(String figurename, int sidesnumber, double side) {
        super(figurename, sidesnumber, side);
    }

    @Override
    public void areaCalculation()
    {
        area = (this.area*this.area)/2;

    }

    @Override
    public void perimeterCalculation()
    {
        perimeter=this.side*3;
    }
    
    @Override
    public void perimeterCalculationTwo()
    {
        perimeter=(this.side+this.side+this.side);
    }

    @Override
    public void radiusCalculation() {

    }

    @Override
    public double areaCalculatorWithRope(double rope, double distanceCenter) {
        return 0;
    }
}

