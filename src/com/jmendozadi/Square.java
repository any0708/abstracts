package com.jmendozadi;

public class Square extends Geometric {


    public Square(double side) {

        super("Square", 4, side);
    }

    @Override
    public void areaCalculation()
    {
        area=this.side*this.side;
    }
    @Override
    public void perimeterCalculation()
    {
        perimeter=this.side*4;
    }
	
	  @Override
    public void perimeterCalculationTwo()
    {
        perimeter=(this.side+this.side+this.side+this.side);
    }

    @Override
    public void perimeterCalculationTwo() {

    }

    @Override
    public void radiusCalculation() {

    }

    @Override
    public double areaCalculatorWithRope(double rope, double distanceCenter) {
        return 0;
    }


}
